namespace SpionshopWebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewNameUsers : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.AspNetUsers", newName: "Klant");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Klant", newName: "AspNetUsers");
        }
    }
}
