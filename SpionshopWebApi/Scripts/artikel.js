﻿// Starturi's voor Web API Services
var artikelsUri = '/api/artikels/';

var SpionshopViewModel = function () {
    // standaardwerkwijze Knockout.js: var self = this;
    // we hebben nu steeds een verwijzing naar het ViewModel in self, this kan veranderen van betekenis
    var self = this;

    // mogelijke foutmelding, bindbaar aan de UI, dus een observable()
    self.error = ko.observable();

    // isEdit: observable om te testen of we in editeermodus of toevoegmodus zijn
    self.isEdit = ko.observable(true);

    // actionDescription: computed observable, titel en knoptekst aanpassen naargelang de modus
    self.actionDescription = ko.computed(function () {
        if (self.isEdit()) return 'Bewerken';
        else return 'Toevoegen';
    });

    // hulpmethode voor het aanspreken van de Service, past indien nodig de errormelding aan.
    self.ajaxHelper = function (uri, method, data) {
        self.error(''); // Foutmelding leegmaken
        return $.ajax({
            type: method,
            url: uri,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).error(function (jqXHR, textStatus, errorThrown) {
            self.error(errorThrown + ': ' + jqXHR.responseText); // Indien fout, foutmelding tonen
        });
    }

    // Lijst met artikelDTO: de lijst met de basisgegevens voor de artikels
    self.artikels = ko.observableArray();

    // detailgegevens van het geslecteerde artikel, zoals getoond in de GUI, dus observable
    self.artikelDetail = ko.observable();

    // De gegevens van ArtikelDTO observable maken
    self.Artikel = function (id, catId, naam, prijs) {
        this.Artikel_id = ko.observable(id),
        this.Cat_id = ko.observable(catId),
        this.Artikel1 = ko.observable(naam),
        this.Verkoopprijs = ko.observable(prijs);
    }

    // artikelDTO: variabele om het ArtikelDTO van de Array artikels te bewaren bij het selecteren van een artikel uit de lijst
    self.artikelDTO = undefined;

    // maak een nieuw ArtikelDetailDTO-object aan: toevoegen van een nieuw artikel
    self.getArtikelDetailNew = function (formElement) {
        var data = {
            Artikel_id: 0,
            Cat_id: 1,
            Artikel1: '',
            Omschrijving: '',
            Verkoopprijs: 0,
            Instock: 0
        }
        self.artikelDetail(data);
        self.isEdit(false);
    }

    // Haal de gegevens van de artikels: lijst van ArtikelDTO in de Array artikels
    self.getArtikels = function (item) {
        self.ajaxHelper(artikelsUri, 'GET').done(function (data) {
            self.artikels(ko.utils.arrayMap(data, function (item) {
                return new self.Artikel(item.Artikel_id, item.Cat_id, item.Artikel1, item.Verkoopprijs);
            }));
        });
    }

    self.getArtikelDetail = function (item) {
        self.artikelDTO = item;
        self.ajaxHelper(artikelsUri + '/GetArtikelsPerId/' + item.Artikel_id(), 'GET').done(function (data) {
            self.artikelDetail(data);
            self.isEdit(true);
        });
    }

    // Sla een artikel op, afhankelijk van isEdit wordt dit een insert of een update
    self.saveArtikel = function (formElement) {
        //zet ko-object om in echt JS-object: properties ipv functieaanroepen
        var saveArtikel = ko.toJS(self.artikelDetail());

        if (self.isEdit()) {
            // update van een Book
            self.ajaxHelper(artikelsUri + self.artikelDetail().Artikel_id, 'PUT', saveArtikel).done(function (item) {
                // Na de update van een Book BookDetailDTO de Array books (BookDTO) weer aanpassen
                // Hier doen we dus geen select-query op de db van de boekenlijst maar passen de Array aan, deze is observable, de GUI wijzigt mee.
                self.artikelDTO.Artikel1(item.Artikel1);
            });
        } else {
            // insert van een Book
            self.ajaxHelper(artikelsUri, 'POST', saveArtikel).done(function (item) {
                var artikel = new self.Artikel(item.Artikel_id, item.Cat_id, item.Artikel1, item.Verkoopprijs);
                self.artikels.push(artikel);
            });
        }
    }

    // verwijder een artikel
    self.deleteArtikel = function (item) {
        self.ajaxHelper(artikelsUri + item.Artikel_id(), 'DELETE').done(function (data) {
            self.artikels.remove(item);
        })
    };

    // initiele data ophalen: de resp. functies uitvoeren
    self.getArtikels();
}

// ViewModel koppelen aan de View
ko.applyBindings(new SpionshopViewModel());