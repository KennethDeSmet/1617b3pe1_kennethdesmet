﻿// Starturi's voor Web API Services
var categoriesUri = '/api/categories/';
var artikelsUri = '/api/artikels/';
var bestellingsUri = 'api/bestellingdetails/'

var SpionshopViewModel = function () {
    // standaardwerkwijze Knockout.js: var self = this;
    // we hebben nu steeds een verwijzing naar het ViewModel in self, this kan veranderen van betekenis
    var self = this;

    // mogelijke foutmelding, bindbaar aan de UI, dus een observable()
    self.error = ko.observable();

    // isEdit: observable om te testen of we in editeermodus of toevoegmodus zijn
    self.isEdit = ko.observable(true);

    // actionDescription: computed observable, titel en knoptekst aanpassen naargelang de modus
    self.actionDescription = ko.computed(function () {
        if (self.isEdit()) return 'Bewerken';
        else return 'Toevoegen';
    });

    // hulpmethode voor het aanspreken van de Service, past indien nodig de errormelding aan.
    self.ajaxHelper = function (uri, method, data) {
        self.error(''); // Foutmelding leegmaken
        return $.ajax({
            type: method,
            url: uri,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).error(function (jqXHR, textStatus, errorThrown) {
            self.error(errorThrown + ': ' + jqXHR.responseText); // Indien fout, foutmelding tonen
        });
    }

    //-----------------------------Categorieën-----------------------------------------------------

    //lijst met categorieDTO: de lijst met de basisgegevens voor de categorieën
    self.categories = ko.observableArray();

    // detailgegevens van het geslecteerde categorie, zoals getoond in de GUI, dus observable
    self.categorieDetail = ko.observable();

    // De gegevens van CategorieDTO observable maken
    self.Categorie = function (id, naam) {
        this.Cat_id = ko.observable(id),
        this.Categorie1 = ko.observable(naam);
    }

    // categorieDTO: variabele om het CategorieDTO van de Array categories te bewaren bij het selecteren van een categorie uit de lijst
    self.categorieDTO = undefined;

    // maak een nieuw CategorieDetailDTO-object aan: toevoegen van een nieuw categorie
    self.getCategorieDetailNew = function (formElement) {
        var data = {
            Cat_id: 0,
            Categorie1: ''
        }
        self.categorieDetail(data);
        self.isEdit(false); // niet in editeer- maar in toevoegmodus
    }

    // Haal de gegevens van de categorieën: lijst van CategorieDTO in de Array categories
    self.getCategories = function () {
        self.ajaxHelper(categoriesUri, 'GET').done(function (data) {
            self.categories(ko.utils.arrayMap(data, function (item) {
                return new self.Categorie(item.Cat_id, item.Categorie1);
            }));
        });
    }

    self.getCategorieDetail = function (item) {
        self.categorieDTO = item;
        self.ajaxHelper(categoriesUri + item.Cat_id(), 'GET').done(function (data) {
            self.categorieDetail(data);
            self.isEdit(true);
        });
    }

    // Sla een categorie op, afhankelijk van isEdit wordt dit een insert of een update
    self.saveCategorie = function (formElement) {
        //zet ko-object om in echt JS-object: properties ipv functieaanroepen
        var saveCategorie = ko.toJS(self.categorieDetail());

        if (self.isEdit()) {
            // update van een Book
            self.ajaxHelper(categoriesUri + self.categorieDetail().Cat_id, 'PUT', saveCategorie).done(function (item) {
                // Na de update van een Book BookDetailDTO de Array books (BookDTO) weer aanpassen
                // Hier doen we dus geen select-query op de db van de boekenlijst maar passen de Array aan, deze is observable, de GUI wijzigt mee.
                self.categorieDTO.Cat_id(item.Cat_id);
                self.categorieDTO.Categorie1(item.Categorie1);
            });
        } else {
            // insert van een Book
            self.ajaxHelper(categoriesUri, 'POST', saveCategorie).done(function (item) {
                var categorie = new self.Categorie(item.Cat_id, item.Categorie1);
                self.categories.push(categorie);
            });
        }
    }

    // verwijder een categorie
    self.deleteCategorie = function (item) {
        self.ajaxHelper(categoriesUri + item.Cat_id(), 'DELETE').done(function (data) {
            self.categories.remove(item);
        })
    };

    //-----------------------------Artikels-----------------------------------------------------

    // Lijst met artikelDTO: de lijst met de basisgegevens voor de artikels
    self.artikels = ko.observableArray();

    // detailgegevens van het geslecteerde artikel, zoals getoond in de GUI, dus observable
    self.artikelDetail = ko.observable();

    // De gegevens van ArtikelDTO observable maken
    self.Artikel = function (id, naam, omschrijving, prijs) {
        this.Artikel_id = ko.observable(id),
        this.Artikel1 = ko.observable(naam),
        this.Omschrijving = ko.observable(omschrijving),
        this.Verkoopprijs = ko.observable(prijs);
    }

    // Haal de gegevens van de artikels per categorie: lijst van ArtikelDTO in de Array artikels
    self.getArtikels = function (item) {
        self.categorieDTO = item;
        self.ajaxHelper(artikelsUri + '/GetArtikelsPerCat/' + item.Cat_id(), 'GET').done(function (data) {
            self.artikels(ko.utils.arrayMap(data, function (item) {
                return new self.Artikel(item.Artikel_id, item.Artikel1, item.Omschrijving, item.Verkoopprijs);
            }));
        });
    }

    // Toon ArtikelDetails in Popup
    self.modalVisible = ko.observable(false);

    self.SelectedModal = ko.observable(self.artikels()[0]);

    self.openModal = function (data, event) {
        self.SelectedModal(data);
        self.modalVisible(true);
    };

    self.closeModal = function (data, event) {
        self.modalVisible(false);
    };

    ko.bindingHandlers.modal = {
        init: function (element, valueAccessors) {
            var options = ko.utils.unwrapObservable(valueAccessors() || {});
            $(element).modal(options);
        },
        update: function (element, valueAccessors) {
            var options = ko.utils.unwrapObservable(valueAccessors() || {});

            $(element).modal(options.show() ? 'show' : 'hide');
        }
    }

    //-----------------------------Bestelling------------------------------------

    // Observable Arrays
    self.bestellingDetails = ko.observableArray();
    self.artikelen = ko.observableArray();

    // Observables
    self.verkoop_btw = ko.observable(0.19);
    self.verzending = ko.observable(5.00);
    self.bestellingDetail = ko.observable(self.artikelen);

    // Class BestelItem 
    var BestelItem = function (artikel, aantal) {
        var self = this;

        self.artikel = ko.observable(artikel);
        self.Aantal = ko.observable(aantal || 1);

        self.totaal = ko.computed(function () {
            return Math.round((self.artikel().Verkoopprijs() * self.Aantal()) * 100) / 100;
        });
    };

    // Computed Observables
    self.subtotaal = ko.computed(function () {
        var subtotaal = 0;
        $(self.artikelen()).each(function (index, cart_item) {
            subtotaal += cart_item.totaal();
        });
        return Math.round(subtotaal * 100) / 100;
    });

    self.btw = ko.computed(function () {
        return Math.round((self.subtotaal() * self.verkoop_btw()) * 100) / 100;
    });

    self.total = ko.computed(function () {
        return Math.round((self.verzending() + self.subtotaal() + self.btw()) * 100) / 100;
    });

    // Voeg artikels toe aan uw bestelling
    self.voegToe = function (artikel, event) {
        var cart_item = new BestelItem(artikel, 1);
        self.artikelen.push(cart_item);
    };

    // Verwijder artikels van uw bestelling
    self.verwijder = function (cart_item, event) {
        self.artikelen.remove(cart_item);
    };

    // Sla een bestelling op naar database
    self.saveBestelling = function (formElement) {
        
        //zet ko-object om in echt JS-object: properties ipv functieaanroepen
        var saveBestelling = ko.toJSON(self.bestellingDetail());

        self.ajaxHelper(bestellingsUri, 'POST', saveBestelling).done(function (item) {
            var bestelling = new BestelItem(item.artikel, item.Aantal)
            self.bestellingDetails.push(bestelling);
        });
    }
    
    // initiele data ophalen: de resp. functies uitvoeren
    self.getCategories();
}

// ViewModel koppelen aan de View
ko.applyBindings(new SpionshopViewModel());