﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SpionshopWebApi.Startup))]
namespace SpionshopWebApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
