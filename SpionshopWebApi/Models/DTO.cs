﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SpionshopWebApi.Models
{
    public class CategorieDTO
    {
        public short Cat_id { get; set; }
        public string Categorie1 { get; set; }
    }

    public class CategorieDetailDTO
    {
        public short Cat_id { get; set; }
        public string Categorie1 { get; set; }
    }

    public class ArtikelDTO
    {
        public short Artikel_id { get; set; }
        public short Cat_id { get; set; }
        public string Artikel1 { get; set; }
        public decimal? Verkoopprijs { get; set; }
    }

    public class ArtikelDetailDTO
    {
        public short Artikel_id { get; set; }
        public short Cat_id { get; set; }
        public string Artikel1 { get; set; }
        public string Omschrijving { get; set; }
        public decimal? Verkoopprijs { get; set; }
        public short? Instock { get; set; }
    }

    public class BestellingDetailDTO
    {
        public int BD_id { get; set; }
        public int B_id { get; set; }
        public short Artikel_id { get; set; }
        public short Aantal { get; set; }
    }
}