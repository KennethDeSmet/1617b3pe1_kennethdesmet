﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace SpionshopWebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "GetArtikelsPerId",
                routeTemplate: "api/{controller}/GetArtikelsPerId/{id}",
                defaults: new { controller = "Artikels", action = "GetArtikelsPerId", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "GetArtikelsPerCat",
                routeTemplate: "api/{controller}/GetArtikelsPerCat/{Cat_id}",
                defaults: new { controller = "Artikels", action = "GetArtikelsPerCat", Cat_id = RouteParameter.Optional }
            );
        }
    }
}
