﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Spionshop.Lib;
using SpionshopWebApi.Models;
using AutoMapper;
using AutoMapper.QueryableExtensions;

namespace SpionshopWebApi.Controllers
{
    public class CategoriesController : ApiController
    {
        private SpionshopContext db = new SpionshopContext();

        public IQueryable<CategorieDTO> GetCategories()
        {
            Mapper.Initialize(cfg => cfg.CreateMap<Categorie, CategorieDTO>());
            var cat = db.Categories.ProjectTo<CategorieDTO>();

            return cat;

        }

        // GET: api/Categories/5
        [ResponseType(typeof(CategorieDetailDTO))]
        public async Task<IHttpActionResult> GetCategorie(short id)
        {
            Mapper.Initialize(cfg => cfg.CreateMap<Categorie, CategorieDetailDTO>());
            var categorie = await db.Categories
                .ProjectTo<CategorieDetailDTO>().SingleOrDefaultAsync(b => b.Cat_id == id);
            if (categorie == null)
            {
                return NotFound();
            }

            return Ok(categorie);
        }

        // PUT: api/Categories/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCategorie(short id, CategorieDetailDTO categorieDetailDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Mapper.Initialize(cfg => cfg.CreateMap<CategorieDetailDTO, Categorie>());
            Categorie categorie = Mapper.Map<Categorie>(categorieDetailDTO);
            db.Set<Categorie>().Attach(categorie);
            db.Entry(categorie).State = EntityState.Modified;
            await db.SaveChangesAsync();

            return Ok(categorieDetailDTO);
        }

        // POST: api/Categories
        [ResponseType(typeof(Categorie))]
        public async Task<IHttpActionResult> PostCategorie(CategorieDetailDTO categorieDetailDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Mapper.Initialize(cfg => cfg.CreateMap<CategorieDetailDTO, Categorie>());
            Categorie categorie = Mapper.Map<Categorie>(categorieDetailDTO);
            db.Categories.Add(categorie);
            await db.SaveChangesAsync();

            // Id wegschrijven naar DTO
            categorieDetailDTO.Cat_id = categorie.Cat_id;

            return Ok(categorieDetailDTO);
        }

        // DELETE: api/Categories/5
        [ResponseType(typeof(Categorie))]
        public async Task<IHttpActionResult> DeleteCategorie(short id)
        {
            Categorie categorie = await db.Categories.FindAsync(id);
            if (categorie == null)
            {
                return NotFound();
            }

            db.Categories.Remove(categorie);
            await db.SaveChangesAsync();

            return Ok(categorie);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CategorieExists(short id)
        {
            return db.Categories.Count(e => e.Cat_id == id) > 0;
        }
    }
}