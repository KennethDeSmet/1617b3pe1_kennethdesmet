﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Spionshop.Lib;
using SpionshopWebApi.Models;
using AutoMapper;

namespace SpionshopWebApi.Controllers
{
    public class BestellingDetailsController : ApiController
    {
        private SpionshopContext db = new SpionshopContext();

        // GET: api/BestellingDetails
        public IQueryable<BestellingDetail> GetBestellingDetails()
        {
            return db.BestellingDetails;
        }

        // GET: api/BestellingDetails/5
        [ResponseType(typeof(BestellingDetail))]
        public async Task<IHttpActionResult> GetBestellingDetail(int id)
        {
            BestellingDetail bestellingDetail = await db.BestellingDetails.FindAsync(id);
            if (bestellingDetail == null)
            {
                return NotFound();
            }

            return Ok(bestellingDetail);
        }

        // PUT: api/BestellingDetails/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutBestellingDetail(int id, BestellingDetail bestellingDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bestellingDetail.BD_id)
            {
                return BadRequest();
            }

            db.Entry(bestellingDetail).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BestellingDetailExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/BestellingDetails
        [ResponseType(typeof(BestellingDetail))]
        public async Task<IHttpActionResult> PostBestellingDetail(BestellingDetailDTO bestellingDetailDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Mapper.Initialize(cfg => cfg.CreateMap<BestellingDetailDTO, BestellingDetail>());
            BestellingDetail bestellingDetail = Mapper.Map<BestellingDetail>(bestellingDetailDTO);
            db.BestellingDetails.Add(bestellingDetail);
            await db.SaveChangesAsync();

            // Id wegschrijven naar DTO
            bestellingDetailDTO.BD_id = bestellingDetail.BD_id;

            return Ok(bestellingDetailDTO);
        }

        // DELETE: api/BestellingDetails/5
        [ResponseType(typeof(BestellingDetail))]
        public async Task<IHttpActionResult> DeleteBestellingDetail(int id)
        {
            BestellingDetail bestellingDetail = await db.BestellingDetails.FindAsync(id);
            if (bestellingDetail == null)
            {
                return NotFound();
            }

            db.BestellingDetails.Remove(bestellingDetail);
            await db.SaveChangesAsync();

            return Ok(bestellingDetail);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BestellingDetailExists(int id)
        {
            return db.BestellingDetails.Count(e => e.BD_id == id) > 0;
        }
    }
}