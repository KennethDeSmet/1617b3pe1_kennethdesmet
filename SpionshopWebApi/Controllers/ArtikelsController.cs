﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Spionshop.Lib;
using SpionshopWebApi.Models;
using AutoMapper;
using AutoMapper.QueryableExtensions;

namespace SpionshopWebApi.Controllers
{
    public class ArtikelsController : ApiController
    {
        private SpionshopContext db = new SpionshopContext();

        // GET: api/Artikels
        public IQueryable<ArtikelDTO> GetArtikels()
        {
            Mapper.Initialize(cfg => cfg.CreateMap<Artikel, ArtikelDTO>());
            var artikel = db.Artikels.ProjectTo<ArtikelDTO>();

            return artikel;
        }

        //GET: api/Artikels/5
        [ActionName("GetArtikelsPerId")]
        [ResponseType(typeof(ArtikelDetailDTO))]
        public async Task<IHttpActionResult> GetArtikelsPerId(short id)
        {
            Mapper.Initialize(cfg => cfg.CreateMap<Artikel, ArtikelDetailDTO>());
            var artikel = await db.Artikels
                .ProjectTo<ArtikelDetailDTO>().SingleOrDefaultAsync(b => b.Artikel_id == id);
            if (artikel == null)
            {
                return NotFound();
            }

            return Ok(artikel);
        }

        // GET: api/Artikels/5
        [ActionName("GetArtikelsPerCat")]
        [ResponseType(typeof(ArtikelDetailDTO))]
        public async Task<IHttpActionResult> GetArtikelsPerCat(short Cat_id)
        {
            Mapper.Initialize(cfg => cfg.CreateMap<Artikel, ArtikelDetailDTO>());
            var artikel = await db.Artikels
                .Where(b => b.Cat_id == Cat_id)
                .ProjectTo<ArtikelDetailDTO>().ToListAsync();
            if (artikel == null)
            {
                return NotFound();
            }

            return Ok(artikel);
        }

        // PUT: api/Artikels/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutArtikel(short id, ArtikelDetailDTO artikelDetailDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Mapper.Initialize(cfg => cfg.CreateMap<ArtikelDetailDTO, Artikel>());
            Artikel artikel = Mapper.Map<Artikel>(artikelDetailDTO);
            db.Set<Artikel>().Attach(artikel); // Voor update
            db.Entry(artikel).State = EntityState.Modified;
            await db.SaveChangesAsync();

            return Ok(artikelDetailDTO);
        }

        // POST: api/Artikels
        [ResponseType(typeof(Artikel))]
        public async Task<IHttpActionResult> PostArtikel(ArtikelDetailDTO artikelDetailDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Mapper.Initialize(cfg => cfg.CreateMap<ArtikelDetailDTO, Artikel>());
            Artikel artikel = Mapper.Map<Artikel>(artikelDetailDTO);
            db.Artikels.Add(artikel);
            await db.SaveChangesAsync();

            // Id naar DTO wegschrijven
            artikelDetailDTO.Artikel_id = artikel.Artikel_id;

            return Ok(artikelDetailDTO);
        }

        // DELETE: api/Artikels/5
        [ResponseType(typeof(Artikel))]
        public async Task<IHttpActionResult> DeleteArtikel(short id)
        {
            Artikel artikel = await db.Artikels.FindAsync(id);
            if (artikel == null)
            {
                return NotFound();
            }

            db.Artikels.Remove(artikel);
            await db.SaveChangesAsync();

            return Ok(artikel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ArtikelExists(short id)
        {
            return db.Artikels.Count(e => e.Artikel_id == id) > 0;
        }
    }
}