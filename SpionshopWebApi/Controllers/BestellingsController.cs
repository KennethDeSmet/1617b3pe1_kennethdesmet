﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Spionshop.Lib;

namespace SpionshopWebApi.Controllers
{
    public class BestellingsController : ApiController
    {
        private SpionshopContext db = new SpionshopContext();

        // GET: api/Bestellings
        public IQueryable<Bestelling> GetBestellings()
        {
            return db.Bestellings;
        }

        // GET: api/Bestellings/5
        [ResponseType(typeof(Bestelling))]
        public async Task<IHttpActionResult> GetBestelling(int id)
        {
            Bestelling bestelling = await db.Bestellings.FindAsync(id);
            if (bestelling == null)
            {
                return NotFound();
            }

            return Ok(bestelling);
        }

        // PUT: api/Bestellings/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutBestelling(int id, Bestelling bestelling)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bestelling.B_id)
            {
                return BadRequest();
            }

            db.Entry(bestelling).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BestellingExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Bestellings
        [ResponseType(typeof(Bestelling))]
        public async Task<IHttpActionResult> PostBestelling(Bestelling bestelling)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Bestellings.Add(bestelling);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = bestelling.B_id }, bestelling);
        }

        // DELETE: api/Bestellings/5
        [ResponseType(typeof(Bestelling))]
        public async Task<IHttpActionResult> DeleteBestelling(int id)
        {
            Bestelling bestelling = await db.Bestellings.FindAsync(id);
            if (bestelling == null)
            {
                return NotFound();
            }

            db.Bestellings.Remove(bestelling);
            await db.SaveChangesAsync();

            return Ok(bestelling);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BestellingExists(int id)
        {
            return db.Bestellings.Count(e => e.B_id == id) > 0;
        }
    }
}