namespace Spionshop.Lib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Bestelling")]
    public partial class Bestelling
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Bestelling()
        {
            BestellingDetails = new HashSet<BestellingDetail>();
        }

        [Key]
        public int B_id { get; set; }

        [Required]
        [StringLength(128)]
        public string Id { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime Datum { get; set; }

        public virtual Klant Klant { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BestellingDetail> BestellingDetails { get; set; }
    }
}
