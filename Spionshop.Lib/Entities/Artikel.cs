namespace Spionshop.Lib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Artikel")]
    public partial class Artikel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Artikel()
        {
            BestellingDetails = new HashSet<BestellingDetail>();
        }

        [Key]
        public short Artikel_id { get; set; }

        public short Cat_id { get; set; }

        [Column("Artikel")]
        [Required]
        [StringLength(50)]
        public string Artikel1 { get; set; }

        [StringLength(500)]
        public string Omschrijving { get; set; }

        [Column(TypeName = "smallmoney")]
        public decimal? Verkoopprijs { get; set; }

        public short? Instock { get; set; }

        public virtual Categorie Categorie { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BestellingDetail> BestellingDetails { get; set; }
    }
}
