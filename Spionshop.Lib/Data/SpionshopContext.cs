namespace Spionshop.Lib
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class SpionshopContext : DbContext
    {
        public SpionshopContext()
            : base("name=SpionshopContext")
        {
        }

        public virtual DbSet<Artikel> Artikels { get; set; }
        public virtual DbSet<Bestelling> Bestellings { get; set; }
        public virtual DbSet<BestellingDetail> BestellingDetails { get; set; }
        public virtual DbSet<Categorie> Categories { get; set; }
        public virtual DbSet<Klant> Klants { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Artikel>()
                .Property(e => e.Artikel1)
                .IsUnicode(false);

            modelBuilder.Entity<Artikel>()
                .Property(e => e.Omschrijving)
                .IsUnicode(false);

            modelBuilder.Entity<Artikel>()
                .Property(e => e.Verkoopprijs)
                .HasPrecision(10, 4);

            modelBuilder.Entity<Artikel>()
                .HasMany(e => e.BestellingDetails)
                .WithRequired(e => e.Artikel)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Bestelling>()
                .HasMany(e => e.BestellingDetails)
                .WithRequired(e => e.Bestelling)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Categorie>()
                .Property(e => e.Categorie1)
                .IsUnicode(false);

            modelBuilder.Entity<Categorie>()
                .HasMany(e => e.Artikels)
                .WithRequired(e => e.Categorie)
                .WillCascadeOnDelete(false);
        }
    }
}
